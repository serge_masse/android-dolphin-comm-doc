## Readme file for android-dolphin-comm-doc ##

#### Serge Masse ####

<!--
This gitlab project contains documentation by the author.

This documentation gitlab project contains the files listed above, and below in this page, the links to relevant web pages, 
the support email address, and the source code license text for all code in the project.
-->

#### GOOGLE PLAY (TM) APP URLS #### 
https://play.google.com/store/apps/details?id=sm.app.dc&hl=en

<p>The source code is now only available on demand in order to minimize potentially hazardous usage of the apps.
</p>

<!-- 
### GITLAB PROJECTS URLS - LIBRARY AND APPLICATIONS ###

#### LIBRARY CODE #### 
https://gitlab.com/leafyseadragon/android-acoustic-lib 

#### APPLICATION CODE #### 
https://gitlab.com/leafyseadragon/android-dolphin-comm-app

https://gitlab.com/leafyseadragon/soundcommandapps 

-->

### CONTACT ### 
sergemasse1@yahoo.com

### PRIVACY POLICY ### 
https://gitlab.com/sergemasse/privacy-policy

### SOURCE CODE LICENSE ###

This code is proprietary since June 29, 2023, 
in order to improve security for cetaceans from potential unethical use.

- Copyright (c) 2023 Serge Masse

